#include <cxxtools/textstream.h>
#include <cxxtools/argin.h>
#include <cxxtools/utf8codec.h>
#include <iostream>
#include <vector>

int main(int argc, char* argv[])
{
    try
    {
        cxxtools::ArgIn in(argc, argv);

        cxxtools::TextIStream tin(in, new cxxtools::Utf8Codec());

        std::vector<int> cc;
        while(true)
        {
            auto c = tin.rdbuf()->sbumpc();
            if (c == std::char_traits<cxxtools::Char>::eof())
                break;
            cc.emplace_back(c);
        }

        for (auto c: cc)
            std::cout << std::hex << c << ' ';
        std::cout << '\n';

        for (auto c: cc)
            std::cout << char(c);
        std::cout << '\n';

        cxxtools::TextOStream tout(std::cout, new cxxtools::Utf8Codec());
        for (auto c: cc)
            tout << cxxtools::Char(c);
        std::cout << '\n';

        //std::cout << tin.rdbuf() << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
}
